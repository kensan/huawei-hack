# Huawei Hack

## Description
Questo codice js (Java script) permette di visualizzare quanto è forte il segnale 4G che riceve il nostro router 4G/4G+ Huawei.

## Visuals
[screenshot di Huawei Hack](/uploads/05cd6dc0e443b4ce5b4e30b9b544dd7f/Screenshot_20221215_022908.png)

In questa immagine si può vedere il semplice risultato di Huawei Hack. Una finestra che è attivata da un segnalibro (bookmark) una volta che siamo entrati nelle impostazioni del nostro router Huawei.

## Installation
Per installare il js (JavaScript) è necessario copiare il codice fornito come fosse un link da memorizzare nei segnalibri. Si tratta in effetti di un lungo link, come può essere http://www.example.com. Io ho un Huawei B535-232 ed è testato con Firefox 102 ma il codice js dovrebbe andare bene su tutti i browser e gli Huawei che usano le API.

## Usage
Per usare questo link o meglio questo js chiamato Huawei hack è necessario loggarsi nella pagina web del proprio router Huawe, non importa in quale pagina si è del router, basta essere loggati. Quindi andare nei segnalibri e cliccare nel link che avete in precedenza memorizzato che supponiamo si chiami Huawei hack.

Facendo un passo indietro, per memorizzare il link Huawei hack nei segnalibri vi rimando al video su YT di MioNonno: https://www.youtube.com/watch?v=ubR6o7iV-qA

## Support
Per avere supporto vi rimando a chi ha sviluppato veramente il codice js, io l'ho solo modificato perché sia più intuitivo. In particolare il canale MioNonno di YT e chi ha scritto veramante il codice. Per quanto riguarda le mie modifiche ho usato i dati limite per definire i 4 tipi di segnale del router secondo le indicazioni di https://opinionitech.com/2021/05/07/rssi-rsrp-rsrq-sinr-cosa-sono-e-come-intepretarli/ che ringrazio. In particolare per chi volesse tradurre il codice in inglese le 4 parole (aggettivi) da tradurre sono:
1. disconnesso / Disconnected
2. insufficiente / Poor
3. buono / Good
4. eccellente / Very Good


## Roadmap
non ho intenzione di sviluppare il codice ad oggi.

## Contributing
Se qualcuno vuole contribuire sarò lieto di offrire le mie poche conoscenze al riguardo

## Authors and acknowledgment
Questo progetto è una modifica minima del codice js di: https://github.com/ECOFRI/HuaweiLTEBandSelector, in particolare della prima parte: https://github.com/ECOFRI/HuaweiLTEBandSelector#show-current-lte-band--signal
Sono arrivato a ECOFRI tramite il canale YT di MioNonno: https://www.youtube.com/@miononno
Ringrazio il sito che mi ha permesso di minimizzare il mio codice: https://minify-js.com/

## License
La licenza è quella ECOFRI che non ha pubblicato la licenza.

## Project status
Questo progetto non è mantenuto.
